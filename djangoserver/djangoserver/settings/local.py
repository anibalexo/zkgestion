
from .base import *

SECRET_KEY = os.environ.get("DJANGO_SECRET_KEY")
DEBUG = True
ALLOWED_HOSTS = ["*"]


#ANTECESOR_BASE_DIR = Path(BASE_DIR).ancestor(1)
#STATIC_URL = '/static/'
#STATICFILES_DIRS = (os.path.join(ANTECESOR_BASE_DIR, 'static'),)
#STATIC_ROOT = os.path.join(ANTECESOR_BASE_DIR, 'staticfiles')