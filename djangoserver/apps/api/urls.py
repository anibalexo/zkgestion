from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenObtainPairView

from .sincronizacion.views import (
    SincronizarTodosDispositivoViewSet,
    ActualizarInfoDispositivoViewSet,
    MigrarInfoTodosDispositivoViewSet,
    SincronizarDispositivo,
    MigrarInfoDispositivoViewSet)

router = DefaultRouter()
#router.register(r'periodo', PeriodoViewSet, basename='periodo')


urlpatterns = [
    path('', include((router.urls, 'api'))),
    path('auth/token/', TokenObtainPairView.as_view(), name="auth-token"),
    path('sincronizar/', SincronizarTodosDispositivoViewSet.as_view(), name="sincronizar-dispositivos"),
    path('migrar/', MigrarInfoTodosDispositivoViewSet.as_view(), name="migrar-info"),
    path('actualizar/info/', ActualizarInfoDispositivoViewSet.as_view(), name="actualizar-info-dispositivo"),
    path('sincronizar/dispositivo/', SincronizarDispositivo.as_view(), name="sincronizar-dispositivo-unico"),
    path('migrar/dispositivo/', MigrarInfoDispositivoViewSet.as_view(), name="migrar-info-unico"),
]

