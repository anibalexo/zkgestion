from rest_framework import views, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication

from ...biometrico.models import Dispositivo
from ...biometrico.utils import sincronizar_empleados, sincronizar_marcaciones, actualizar_info_dispositivo
from ...biometrico.zk_py import const
from ...biometrico.zk_py.exception import ZKError
from ...biometrico.querys.crud import migrar_registros


def get_privilegio(user):
    privilege = "User"
    if user.privilege == const.USER_ADMIN:
        privilege = 'Admin'
    return privilege


class SincronizarTodosDispositivoViewSet(views.APIView):
    """
    Sincronizamos registros de todos los dispositivos
    biometricos activos a base de integración
    """
    authentication_classes = (JWTTokenUserAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        dispositivos = Dispositivo.objects.filter(activo=True)
        rsp = []
        for dsp in dispositivos:
            try:
                sincronizar_empleados(dsp)
                sincronizar_marcaciones(dsp)
                rsp.append({'estado': 'ok', 'descripcion': 'Sincronización realizada con éxito', 'dispositivo': dsp.nombre})
            except ZKError as e:
                rsp.append({'estado': 'error', 'descripcion': str(e), 'dispositivo': dsp.nombre})
        return Response(rsp, status.HTTP_200_OK)


class ActualizarInfoDispositivoViewSet(views.APIView):
    """
    Consultamos y Actualizamos informacion de un dispositivo
    """
    authentication_classes = (JWTTokenUserAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        dispositivo_id = self.request.query_params.get('dispositivo_id', None)
        if dispositivo_id is None:
            return Response({'error': 'No se encontro el id dispositivo'}, status.HTTP_400_BAD_REQUEST)
        rsp = []
        try:
            dispositivo = Dispositivo.objects.get(pk=dispositivo_id)
            actualizar_info_dispositivo(dispositivo)
            rsp.append({'estado': 'ok', 'descripcion': 'Sincronización realizada con éxito', 'dispositivo': dispositivo.nombre})
        except ZKError as e:
            rsp.append({'estado': 'error', 'descripcion': str(e), 'dispositivo': dispositivo.nombre})
        return Response(rsp, status.HTTP_200_OK)


class MigrarInfoTodosDispositivoViewSet(views.APIView):
    """
    Migra marcaciones de base de integracion a base
    attendance sql server
    """
    authentication_classes = (JWTTokenUserAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        dispositivos = Dispositivo.objects.filter(activo=True)
        rsp = []
        for dsp in dispositivos:
            try:
                periodos = dsp.periodosdispositivo.filter(migrado=False)
                for per in periodos:
                    marcaciones = per.marcacionesPeriodo.all()
                    migrar_registros(marcaciones, per)
                rsp.append({'estado': 'ok', 'descripcion': 'Migración realizada con éxito', 'dispositivo': dsp.nombre})
            except Exception as e:
                rsp.append({'estado': 'error', 'descripcion': str(e), 'dispositivo': dsp.nombre})
        return Response(rsp, status.HTTP_200_OK)


class SincronizarDispositivo(views.APIView):
    """
    Sincroniza informacion desde un dispositivo
    """
    authentication_classes = (JWTTokenUserAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        dispositivo_id = self.request.query_params.get('dispositivo_id', None)
        if dispositivo_id is None:
            return Response({'error': 'No se envio el id dispositivo'}, status.HTTP_400_BAD_REQUEST)
        rsp = []
        try:
            dispositivo = Dispositivo.objects.get(pk=dispositivo_id)
            sincronizar_empleados(dispositivo)
            sincronizar_marcaciones(dispositivo)
            rsp.append({'estado': 'ok', 'descripcion': 'Sincronización realizada con éxito', 'dispositivo': dispositivo.nombre})
        except ZKError as e:
            rsp.append({'estado': 'error', 'descripcion': str(e), 'dispositivo': dispositivo.nombre})
        return Response(rsp, status.HTTP_200_OK)


class MigrarInfoDispositivoViewSet(views.APIView):
    """
    Migra marcaciones de un dispositivo a
    base attendance sql server
    """
    authentication_classes = (JWTTokenUserAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        dispositivo_id = self.request.query_params.get('dispositivo_id', None)
        if dispositivo_id is None:
            return Response({'error': 'No se envio el id dispositivo'}, status.HTTP_400_BAD_REQUEST)
        rsp = []
        try:
            dispositivo = Dispositivo.objects.get(pk=dispositivo_id)
            periodos = dispositivo.periodosdispositivo.filter(migrado=False)
            for per in periodos:
                marcaciones = per.marcacionesPeriodo.all()
                migrar_registros(marcaciones, per)
            rsp.append({'estado': 'ok', 'descripcion': 'Migración realizada con éxito', 'dispositivo': dispositivo.nombre})
        except ZKError as e:
            rsp.append({'estado': 'error', 'descripcion': str(e), 'dispositivo': dispositivo.nombre})
        return Response(rsp, status.HTTP_200_OK)
