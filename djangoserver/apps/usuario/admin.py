from django.contrib import admin

from .models import User
from django.contrib.auth.admin import UserAdmin


class UsuarioAdmin(UserAdmin):
    """
    Model admin para dispositivos biometricos
    """
    list_display = ('email', 'username', 'first_name', 'last_name', 'is_active', 'last_login')
    readonly_fields = ('date_joined', 'last_login')
    fieldsets = (
        (None, {'fields': ('email', 'username', 'password',)}),
        ('Información', {'fields': ('first_name', 'last_name')}),
        ('Permisos', {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        ('Fechas Importantes', {'fields': ('last_login', 'date_joined')})
    )


admin.site.register(User, UsuarioAdmin)



