from django.db import connections

from ..models import Periodo


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def obtener_periodos():
    periodos = Periodo.objects.filter(migrado=False, dispositivo__pk=2)
    for per in periodos:
        marcaciones = per.marcacionesPeriodo.all()[:10]
        migrar_registros(marcaciones, per)


def migrar_registros(marcaciones, periodo):
    """
    Crea registros en base de datos sql server
    :param marcaciones: registros de marcacion en biometrico
    :param periodo: periodo para las marcaciones
    :return: boolean
    """
    cursor = connections['faces_db'].cursor()
    try:
        query = "INSERT INTO CHECKINOUT (USERID, CHECKTIME, CHECKTYPE, VERIFYCODE, SENSORID, WorkCode, MEMOINFO, COD_CARGA, ARCHIVO) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        data = [(mrc.empleado.id_user, mrc.fecha, 'I', 1, str(mrc.dispositivo.pk), 1, None, str(periodo.pk), 'zkgestion') for
                mrc in marcaciones]
        cursor.executemany(query, data)
        cursor.commit()
    finally:
        periodo.migrado = True
        periodo.save()
        cursor.close()
    return True




