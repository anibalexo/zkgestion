from django.db import models

from .dispositivo import Dispositivo
from .empleado import Empleado
from .periodo import Periodo


class Marcacion(models.Model):
    """
    Registro de marcación realizada en dispositivo biometrico
    """
    fecha = models.DateTimeField()
    tipo = models.IntegerField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)
    periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE, related_name='marcacionesPeriodo')
    dispositivo = models.ForeignKey(Dispositivo, on_delete=models.CASCADE)
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'marcacion'
        verbose_name_plural = 'marcaciones'
        ordering = ['-fecha']

    def __str__(self):
        return "{} - {}".format(self.fecha, self.empleado)
