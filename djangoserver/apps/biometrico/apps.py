from django.apps import AppConfig


class BiometricoConfig(AppConfig):
    name = 'biometrico'
