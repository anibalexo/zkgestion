# ZKGESTION

ZKGESTION is a project for extracting user markings from ZK biometric devices

### Docker container access
```sh
docker-compose exec -it <id contenedor> sh
```

### Development environment
To build the image and execute the project under development the following commands are used:
```sh
docker-compose -f local.yml build
docker-compose -f local.yml up -d
```

To force construction of images without cache, use the following command:
```sh
docker-compose -f local.yml build --no-cache
```

Create file for environment variables in `.env` directory with name `.env_dev`, in this file define the variables

GLOBAL_VARIABLES: 
django secret key and settings file for docker compose
> DJANGO_SETTINGS_MODULE=djangoserver.settings.local
> 
> DJANGO_SECRET_KEY=`secret key django`
> 

POSTGRESQL
postgresql access credentials
> POSTGRES_DB=`name database`
> 
> POSTGRES_USER=`user database`
> 
> POSTGRES_PASSWORD=`password database`
> 
> POSTGRES_HOST=`ip database`
> 
> POSTGRES_PORT=5432

CONFIG EMAIL
email settings for notifications
> EMAIL_ACCOUNT=`account email`
> 
> EMAIL_PASSWORD=`passsword email`

    
### Production environment
To build the image and run the project in production the following commands are used:
```sh
docker-compose -f production.yml build
docker-compose -f production.yml up -d
```

